#!/bin/bash

tr_suffix="ttex"
tr2tex_prog="./utils/turkish2tex.pl"
master_tex_file=matematik1.tex

for f in $tr2tex_prog
do
	if test ! -f $f ; then
		echo "$0: File $f does not exist!"
		exit 1
	fi
done

for file in *.ttex
do
	file_prefix=${file%.*}
	tr_file="${file_prefix}.$tr_suffix"
	tex_file="${file_prefix}.tex"
	cat $tr_file | $tr2tex_prog > $tex_file
#	tex $tex_file
done

tex $master_tex_file

if test $? -eq 0 ; then
	#pdftex $master_tex_file
	file_prefix=${master_tex_file%.*}
	master_dvi_file=${file_prefix}.dvi
	master_ps_file=${file_prefix}.ps
	dvips $master_dvi_file
	ps2pdf $master_ps_file
fi
