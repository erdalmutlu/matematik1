#!/usr/bin/perl -w
#
#    Copyright (C) Erdal Mutlu
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
#######################################################################################
#BEGIN {push @INC, '..'}
## or run : perl -I..
use strict;
use warnings;
use utf8;

binmode(STDIN,  ":utf8");
binmode(STDOUT, ":utf8");
binmode(STDERR, ":utf8");

foreach my $line (<STDIN>) {
	$line = turkish2tex($line);
	print $line;
}
exit 0;

sub turkish2tex
{
	my $tr_str = shift;

	my $tex_str = $tr_str;

	$tex_str =~ s/ç/\\c c/g;
	$tex_str =~ s/Ç/\\c C/g;
	
	$tex_str =~ s/ş/\\c s/g;
	$tex_str =~ s/Ş/\\c S/g;
	
	$tex_str =~ s/ö/\\"o/g;
	$tex_str =~ s/Ö/\\"O/g;
	
	$tex_str =~ s/ü/\\"u/g;
	$tex_str =~ s/Ü/\\"U/g;
	
	$tex_str =~ s/ğ/\\u g/g;
	$tex_str =~ s/Ğ/\\u G/g;
	
	$tex_str =~ s/ı /\\i\\ /g;
	$tex_str =~ s/ı/\\i /g;
	$tex_str =~ s/İ/\\.I/g;

	return $tex_str;
}
